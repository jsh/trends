"""Unit tests for random number generators."""
# Prevent complaints about fixtures.
#   pylint: disable=redefined-outer-name
# TODO: test exception handling

import os
import random

import pytest

from trends import CONFIG_FN, DEFAULT_CONFIG, _gen_random, configure_dist


def test_config_random_default() -> None:
    """Default distribution is correct."""
    default = DEFAULT_CONFIG["RAND"]
    assert configure_dist("RAND") == CONFIG_FN["RAND"][default]


@pytest.mark.parametrize(
    "rand_dist_name, rand_dist_fn",
    [
        ("uniform", random.uniform),
        ("normal", random.normalvariate),
        ("lognormal", random.lognormvariate),
    ],
)
def test_config_rand(rand_dist_name, rand_dist_fn) -> None:
    """Random distributions can be configured."""
    os.environ["RAND"] = rand_dist_name
    assert configure_dist("RAND") == rand_dist_fn
    os.environ["RAND"] = DEFAULT_CONFIG["RAND"]  # reset to default


def test_rands_differ() -> None:
    """Each random number is different."""
    nrands = 100
    rands = list(_gen_random(nrands))
    assert nrands == len(rands)
    assert len(set(rands)) == len(rands)


def test_rand_range() -> None:
    """All random numbers are in range."""
    nrands = 100
    rands = list(_gen_random(nrands))
    out_of_range = [rand for rand in rands if (rand < 0 or rand > 1)]
    assert not out_of_range
