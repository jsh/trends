# Welcome to the Documentation for Trends!

```{warning}
This library is under active development.
```

See {ref}`Installation` for installation instructions and
{ref}`A Trends Tutorial` for a tutorial.

```{include} ../../../README.md
:relative-images:
```

```{toctree}
   :caption: Contents
   :maxdepth: 2

installation
api
notebooks/Trends_Tutorial
```
