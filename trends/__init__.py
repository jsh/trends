"""Define and manipulate trends."""
from .trends import *  # noqa: F401,F403
from .trends import _average_merge, _gen_random  # noqa: F401
