#!/bin/sh
# Run a series of checks, stopping at the first error.
# Don't let shellcheck whine about unquoted variables.
#     shellcheck disable=SC2086

set -u                                  # error to refer to unset vars (catches typos)
set -e                                  # stop at the first error
# set -o pipefail                       # fail if a pipeline step fails, missing in some POSIX shells

die() { echo "$@"; exit 1; }            # print args and die
usage="usage: $0 [-i] [file ...]"

# process options
while [ "$*" ]; do                      # too lazy to use getopts
	case "${1:-}" in
		-i|--ignore) set +e     # ignore errors, run all checks
		shift
		;;
		-d|--debug) set -x      # watch what the script's doing
		shift
		;;
		-*) die $usage;;        # no other options permitted
		 *) break ;;            # make $@, $*, be "the rest of the args"
	esac
done

python_files=$(
	find . -name '*.py'             # default to checking all Python files
)
excludes=trends/docs/source/conf.py     # whoops
python_files=$(ls $python_files | grep -v $excludes)
files="${*:-$python_files}"             # use file names on command line if they're there

perl -i -pe s'/\r$//' $files            # de-DOS
isort --profile black $files            # sort the imports
black $files                            # make it look nice

f8_files=$(ls trends/*.py | grep -v __init__.py)
f8_flags="--max-line-length 88"                     # gotta stop somewhere
f8_flags="$f8_flags --max-complexity 4"             # check McCabe complexity
f8_flags="$f8_flags --docstring-convention google"  # lint docstrings
flake8 $f8_flags $f8_files                 # basic checks

mypy_flags="--namespace-packages"                   # support namespace packages (probably don't need this)
mypy_flags="$mypy_flags --explicit-package-bases"   # modules are local or in $MYPATH
mypy_flags="$mypy_flags --show-error-codes"         # in case I want to know what to ignore :-)
mypy_flags="$mypy_flags --exclude source/conf.py" # in case I want to know what to ignore :-)
mypy $mypy_flags $files                 # optional type checking

# pylint -rn $files                       # the pickiest linter I've found so far. "-rn" for "recursive, not-chatty"
bandit -ll -r -q .                      # check for security vulnerabilies (severity Medium or High). "recursive, not chatty"
safety check                            # report security vulnerabilities in dependencies. Wish there were a "not chatty"

# Announce completion. Probably optional, but what the heck.
printf "\n\n        == %s complete! ==\n\n" "$(basename $0)"
