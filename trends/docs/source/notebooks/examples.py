import statistics
from random import random

try:
    import blessed
except ImportError:
    pass


def is_trend(s):
    # breakpoint()
    n = len(s)
    for i in range(1, n):
        if statistics.mean(s[:i]) > statistics.mean(s[i:]):
            return False
    return True


def rand(n):
    return [random() for elem in range(n)]  # a sequence of n random reals in [01)


def mono(n):
    return sorted(rand(n))  # a monotonically increasing arrangement of the same numbers


def is_mono_inc(s):
    return all([s[i] > s[i - 1] for i in range(1, len(s))])


def is_mono_inc2(s):
    n = len(s)
    for i in range(1, n):
        if max(s[:i]) > max(s[i:]):
            return False
    return True


def pfx_trend(s):
    t = s.copy()
    while t:
        if is_trend(t):
            return t
        t.pop()


def trendlist(s):
    trendlist = []
    while s:
        p = pfx_trend(s)
        trendlist.append(p)
        p_len = len(p)
        s = s[p_len:]
    return trendlist


def print_trendlist(trendlist):
    term = blessed.Terminal()
    for i, trend in enumerate(trendlist, start=1):
        trend_l = [f"{elem:0.2f}" for elem in trend]
        trend_s = ",".join(trend_l)
        trend_p = "[" + trend_s + "]"
        print(term.color(i)(trend_p), end="")
    print


def print_trends(s):
    tl = trendlist(s)
    print_trendlist(tl)


def single_trend(s):
    ts = trendlist(s)
    if len(ts) == 1:
        return s
    mean_s = statistics.mean(s)
    rotate = 0
    for t in ts:
        mean_t = statistics.mean(t)
        if mean_t < mean_s:
            return s[rotate:] + s[:rotate]
        rotate += len(t)


def trend_lengths(s):
    ts = trendlist(s)
    return [len(t) for t in ts]


def ave_pfx_len(len_s):
    len_tot = 0
    trials = 128
    for _ in range(trials):
        s = rand(len_s)
        p = pfx_trend(s)
        len_tot += len(p)
    return len_tot / trials


def pfx_len_by_seq_len(len_max):
    len_s = 1
    pl_x_sl = {}
    while len_s < len_max:
        pl_x_sl[len_s] = ave_pfx_len(len_s)
        len_s *= 2
    return pl_x_sl


def ave_ntrends(len_s):
    ntrends_tot = 0
    trials = 128
    for _ in range(trials):
        s = rand(len_s)
        tl = trendlist(s)
        ntrends_tot += len(tl)
    return ntrends_tot / trials


def ntrends_by_seq_len(len_max):
    len_s = 1
    nl_x_sl = {}
    while len_s < len_max:
        nl_x_sl[len_s] = ave_ntrends(len_s)
        len_s *= 2
    return nl_x_sl
