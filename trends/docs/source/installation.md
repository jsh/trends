# Installation
You can install Trends directly from PyPI.

```console
$ python -m venv .venv
$ source .venv/bin/activate
(.venv) $ pip install trends
```

(I recommend installing into a virtual environment: `venv`, `pipenv`, `poetry`, ... whatever.)
