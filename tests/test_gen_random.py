"""Unit test random number generator."""
# Prevent complaints about fixtures.
#   pylint: disable=redefined-outer-name

import random
from typing import List

import pytest

from trends import _gen_random

FIXED_SEED = 1.0


@pytest.fixture(scope="function")  # TODO: make this global to the tests
def rnum_fixed() -> float:
    """Return random number from fixed seed."""
    random.seed(FIXED_SEED)
    return random.random()


@pytest.fixture(scope="function")
def rands_69() -> List[float]:
    """Return sequence of 69 random numbers from a fixed seed."""
    return list(_gen_random(seed=FIXED_SEED, nrands=69))


def test_gen_random_seed(rands_69, rnum_fixed) -> None:
    """Test seed parameter."""
    assert list(_gen_random(seed=FIXED_SEED)) == [rnum_fixed]
    assert rands_69[0] == rnum_fixed
    assert list(_gen_random()) != [rnum_fixed]
    assert list(_gen_random(seed=3)) != [rnum_fixed]


def test_gen_random_nrands(rands_69):
    """Test nrands parameter."""
    assert len(rands_69) == 69
    singleton = list(_gen_random(seed=FIXED_SEED))
    assert len(singleton) == 1


def test_gen_random_rot(rands_69):
    """Test rot parameter."""
    rotated_30 = list(_gen_random(seed=FIXED_SEED, nrands=69, rot=30))
    assert len(rotated_30) == 69
    assert rotated_30 == rands_69[30:] + rands_69[:30]


def test_gen_random_rot_mod(rands_69):
    """Test that rot > nrands works."""
    assert list(_gen_random(seed=FIXED_SEED, nrands=69, rot=69)) == rands_69
    assert list(_gen_random(seed=FIXED_SEED, nrands=69, rot=70)) != rands_69
    assert list(_gen_random(seed=FIXED_SEED, nrands=69, rot=70)) == list(
        _gen_random(seed=FIXED_SEED, nrands=69, rot=1)
    )
    assert list(_gen_random(seed=FIXED_SEED, nrands=69, rot=-68)) == list(
        _gen_random(seed=FIXED_SEED, nrands=69, rot=1)
    )
