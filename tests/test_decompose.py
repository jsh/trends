"""Unit tests for trends."""
# Prevent complaints about fixtures.
#   pylint: disable=redefined-outer-name

from math import pi

from trends import Trend, Trendlist, decompose

FLT_1 = pi
LST_1 = [FLT_1]
TR_1 = Trend(FLT_1)
TL_1 = Trendlist([TR_1])


def test_trivial_decompose() -> None:
    """Unit-test decompose."""
    assert decompose([]) == ([], [])
    assert decompose(LST_1) == (TL_1, TL_1)
