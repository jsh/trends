API
===

.. autosummary::
   :toctree: generated

   trends

.. autofunction:: trends.random_trends

.. autoclass:: trends.Trend
   :members:

.. autoclass:: trends.Trendlist
   :members:
