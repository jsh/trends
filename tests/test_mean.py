"""Unit tests for means."""
# Prevent complaints about fixtures.
#   pylint: disable=redefined-outer-name

import os
from math import e, pi
from statistics import geometric_mean, harmonic_mean, mean
from typing import Tuple

import pytest

from trends import (
    CONFIG_FN,
    DEFAULT_CONFIG,
    Trend,
    a_mean,
    configure_dist,
    g_mean,
    h_mean,
)


def test_config_mean_default() -> None:
    """Default mean is correct."""
    default = DEFAULT_CONFIG["MEAN"]
    assert configure_dist("MEAN") == CONFIG_FN["MEAN"][default]


# TODO: check on exceptions


@pytest.mark.parametrize(
    "mean_name, mean_fn",
    [
        ("arithmetic", a_mean),
        ("geometric", g_mean),
        ("harmonic", h_mean),
    ],
)
def test_config_mean(mean_name, mean_fn) -> None:
    """Mean function can be configured."""
    os.environ["MEAN"] = mean_name
    assert configure_dist("MEAN") == mean_fn
    os.environ["MEAN"] = DEFAULT_CONFIG["MEAN"]  # reset to default


@pytest.fixture()
def trend_pair() -> Tuple["Trend", "Trend"]:
    """Return a standard trend pair."""
    trend1 = Trend(length=10, average=pi)
    trend2 = Trend(length=20, average=e)
    return trend1, trend2


def test_a_mean(trend_pair: Tuple) -> None:
    """Arithmetic mean function returns correct answer."""
    trend1, trend2 = trend_pair
    combined_mean = mean(
        trend1.length * [trend1.average] + trend2.length * [trend2.average]
    )
    assert a_mean(trend1, trend2) == pytest.approx(combined_mean, 10**-16)


def test_g_mean(trend_pair: Tuple) -> None:
    """Geometric mean function returns correct answer."""
    trend1, trend2 = trend_pair
    combined_mean = geometric_mean(
        trend1.length * [trend1.average] + trend2.length * [trend2.average]
    )
    assert g_mean(trend1, trend2) == pytest.approx(combined_mean, 10**-16)


def test_h_mean(trend_pair: Tuple) -> None:
    """Harmonic mean function returns correct answer."""
    trend1, trend2 = trend_pair
    combined_mean = harmonic_mean(
        trend1.length * [trend1.average] + trend2.length * [trend2.average]
    )
    assert h_mean(trend1, trend2) == pytest.approx(combined_mean, 10**-16)
